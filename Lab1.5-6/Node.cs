﻿using System;

namespace Lab1._5_6
{
    public class Node<TKey, TValue>
        where TKey : IComparable
        where TValue : IComparable
    {
        public Node(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        public TKey Key { get; set; }
        public TValue Value { get; set; }
        public Node<TKey, TValue> LSon { get; set; }
        public Node<TKey, TValue> RSon { get; set; }

        public static implicit operator string(Node<TKey, TValue> node)
        {
            return node.Key + " " + node.Value;
        }

        public static explicit operator Node<TKey, TValue>(string datas)
        {
            var dataConvertor=new ConverterForGenericsData();
            string[] inputDatas = datas.Split();
            if (inputDatas.Length >= 2)
            {
                if (dataConvertor.TryConvert(inputDatas[0], out TKey addedKey) &&
                    dataConvertor.TryConvert(inputDatas[1], out TValue value))
                    return new Node<TKey, TValue>(addedKey, value);
            }

            return null;
        }
    }
}
