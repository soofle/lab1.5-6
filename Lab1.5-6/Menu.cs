﻿using System;

namespace Lab1._5_6
{
    public static class Menu
    {
        public static void Start()
        {
            Console.WriteLine("Добро пожаловать в Задачу 10 \"Бинарное дерево поиска, хранящее строки\"");

            var binTree = new BinaryTree<string, int>();
            binTree.TreeChanged += binTree.ShowTree;
            var dataConvertor = new ConverterForGenericsData();

            bool end = false;
            while (end != true)
            {
                Console.WriteLine("\n"+"Выберите действие: \n" +
                                  "1. Добавить узел в дерево \n" +
                                  "2. Удалить узел из дерева \n" +
                                  "3. Вывести дерево на экран \n" +
                                  "4. Заполнить дерево из файла \n" +
                                  "9. Выйти");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Введите ключ и значение");
                        string[] inputDatas = Console.ReadLine().Split();
                        if (inputDatas.Length >= 2)
                        {
                            if (dataConvertor.TryConvert(inputDatas[0], out string addedKey) &&
                                dataConvertor.TryConvert(inputDatas[1], out int value))
                                binTree.Insert(addedKey, value);
                        }
                        else
                        {
                            Console.WriteLine("Input at least two arguments");
                        }

                        break;
                    case "2":
                        Console.WriteLine("Введите удаляемый ключ");
                        inputDatas = Console.ReadLine().Split();
                        if (dataConvertor.TryConvert(inputDatas[0], out string removedKey))
                            binTree.Remove(removedKey);
                        break;
                    case "3":
                        binTree.ShowTree(new object(), new EventArgs());
                        break;
                    case "4":
                        binTree.FillTreeFromFile("TreeElements.txt");
                        break;
                    case "9":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Введите верное значение");
                        break;
                }
            }
        }
    }
}
