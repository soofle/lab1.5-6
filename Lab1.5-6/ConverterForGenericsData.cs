﻿using System;
using System.ComponentModel;

namespace Lab1._5_6
{
    public class ConverterForGenericsData
    {
        public bool TryConvert<TVariable>(string data,out TVariable result)
        {
            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(TVariable));
                if (converter != null)
                {
                    result=(TVariable) converter.ConvertFromString(data);
                    return true;
                }
                result = default(TVariable);
                return false;
            }
            catch (FormatException formatException)
            {
                Console.WriteLine(formatException.Message);
                result = default(TVariable);
                return false;
            }
            catch (NotSupportedException notSupportedException)
            {
                Console.WriteLine(notSupportedException.Message);
                result = default(TVariable);
                return false;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result = default(TVariable);
                return false;
            }
        }
    }
}
