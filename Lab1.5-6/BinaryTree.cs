﻿using System;
using System.IO;

namespace Lab1._5_6
{
    public class BinaryTree<TKey, TValue>
        where TKey : IComparable
        where TValue : IComparable
    {
        private Node<TKey, TValue> Root { get; set; }
        public event EventHandler TreeChanged;

        public void Insert(TKey key, TValue value)
        {
            InsertToTree(key, value);
            OnTreeChanged(EventArgs.Empty);
        }

        private void InsertToTree(TKey key, TValue value)
        {
            if (Root == null)
            {
                Root = new Node<TKey, TValue>(key, value);
                return;
            }

            InsertTo(Root, key, value);
        }

        private void InsertTo(Node<TKey, TValue> node, TKey key, TValue value)
        {
            if (key.CompareTo(node.Key) < 0)
            {
                if (node.LSon == null)
                {
                    node.LSon = new Node<TKey, TValue>(key, value);
                    return;
                }

                InsertTo(node.LSon, key, value);
                return;
            }

            if (key.CompareTo(node.Key) > 0)
            {
                if (node.RSon == null)
                {
                    node.RSon = new Node<TKey, TValue>(key, value);
                    return;
                }

                InsertTo(node.RSon, key, value);
                return;
            }

            node.Value = value;
        }

        public Node<TKey, TValue> Minimum(Node<TKey, TValue> node)
        {
            if (node.LSon == null)
                return node;
            return Minimum(node.LSon);
        }

        public void Remove(TKey key)
        {
            Root = RemoveFrom(Root, key);
            OnTreeChanged(EventArgs.Empty);
        }

        private Node<TKey, TValue> RemoveFrom(Node<TKey, TValue> root, TKey key)
        {
            if (root == null)
                return null;
            if (key.CompareTo(root.Key) < 0)
                root.LSon = RemoveFrom(root.LSon, key);
            else
            {
                if (key.CompareTo(root.Key) > 0)
                    root.RSon = RemoveFrom(root.RSon, key);
                else
                {
                    if (root.RSon != null && root.LSon != null)
                    {
                        root.Value = Minimum(root.RSon).Value;
                        root.Key = Minimum(root.RSon).Key;
                        root.RSon = RemoveFrom(root.RSon, root.Key);
                    }
                    else
                    {
                        if (root.LSon != null)
                            root = root.LSon;
                        else
                            root = root.RSon;
                    }
                }
            }

            return root;
        }

        public void ShowTree(object sender, EventArgs e)
        {
            if (Root != null)
            {
                Console.WriteLine("\n" + "Our tree:");
                Traverse(Root);
            }
            else
            {
                Console.WriteLine("\n" + "Tree is empty");
            }
        }

        private void Traverse(Node<TKey, TValue> root)
        {
            if (root == null)
                return;

            if (root.LSon != null)
                Traverse(root.LSon);
            PrintNode(root);
            if (root.RSon != null)
                Traverse(root.RSon);
        }

        private void PrintNode(Node<TKey, TValue> node)
        {
            Console.WriteLine("{0} - {1}", node.Key, node.Value);
        }

        public void FillTreeFromFile(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("File not found");
                return;
            }

            var dataConvertor = new ConverterForGenericsData();

            string[] allLines = File.ReadAllLines(path);
            foreach (string line in allLines)
            {
                if (!string.IsNullOrWhiteSpace(line))
                {
                    string[] datas = line.Split();
                    if (datas.Length >= 2)
                    {
                        if (dataConvertor.TryConvert(datas[0], out TKey key) &&
                            dataConvertor.TryConvert(datas[1], out TValue value))
                        {
                            InsertToTree(key, value);
                        }
                    }
                }
            }

            OnTreeChanged(EventArgs.Empty);
        }

        public virtual void OnTreeChanged(EventArgs e)
        {
            if (TreeChanged != null)
                TreeChanged(this, e);
        }
    }
}
